This is for use in tutorial 8 in Software Engineering Workshop. 

It contains two packages that require refactoring. 
- *for_refactoring* - basic tasks - a monolithic class and method that lacks cohesion
- *morerefactoring* - advanced task - another monolithic class and method that lacks any sort of self-describing elements in the code. It's been obfuscated one way or another - including the name of the primary class!

As this is a tutorial source project, merge requests will be ignored.
